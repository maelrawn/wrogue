//All UI elements should be in the range of grid space 56-80;
//this is inside the default terminal size, and outside of the play area.


#include "PlayerClass.hpp"
#include <string>
using std::string;



void clearScreen(){	
	for(int x = 0; x < 24; x++)
		for(int y = 0; y < 54; y++)
			mvaddch(y, x, ' ');
}

void drawFullscreenBorder(char graphic){
	for(int x = 0; x < 54; x++)	//Top and bottom borders
		mvaddch(0, x, graphic);
	for(int x = 0; x < 54; x++)
		mvaddch(23, x, graphic);

	for(int y = 0; y < 24; y++)	//Left and right borders
		mvaddch(y, 0, graphic);
	for(int y = 0; y < 24; y++)
		mvaddch(y, 54, graphic);
}

void clearUIPanel(){
	for(int x = 0; x < 24; x++)
	mvprintw(0, 56, "                    ");
}

void drawUIDivisor(){
	for(int y = 0; y < 24; y++)
		mvaddch(y, 55, '|');
}

void drawCombatLog(){
	mvprintw(16, 56, "-------Combat Log-------");
}

void displayGeneral(Player player){
	string name, level;
	level = to_string(player.getLevel());
	name = player.getName();
	const char *lvl = level.c_str();
	const char *nme = name.c_str();
	mvprintw(0, 65, nme);
	mvprintw(0, 56, "Level ");
	mvprintw(0, 62, lvl);
}

void displayTime(int turnCounter){
	string time;
	time = to_string(turnCounter);
	const char * tme = time.c_str();
	mvprintw(2, 56, "Turn No. ");
	mvprintw(2, 65, tme);
}

void displayHealth(Player player){
	string currenthealth, maxhealth;
	currenthealth = to_string(player.getCurrentHealth());
	maxhealth = to_string(player.getMaxHealth());
	const char *cmh = maxhealth.c_str();
	const char *cch = currenthealth.c_str();
	mvprintw(4, 56, "HP:");
	mvprintw(4, 60, cch);
	mvprintw(4, 63, "/");
	mvprintw(4, 64, cmh);
}

void displayMana(Player player){
	string currentmana, maxmana;
	currentmana = to_string(player.getCurrentMana());
	maxmana = to_string(player.getMaxMana());
	const char *ccm = currentmana.c_str();
	const char *cmm = maxmana.c_str();
	mvprintw(4, 68, "Mana:");
	mvprintw(4, 73, ccm);
	mvprintw(4, 76, "/");
	mvprintw(4, 77, cmm);
}

void displayXP(Player player){
	string currentXP, nextXP;
	currentXP = to_string(player.getXP());
	nextXP = to_string(player.getNXP());
	const char *cxp = currentXP.c_str();
	const char *nxp = nextXP.c_str();
	mvprintw(6, 56, "XP:");
	mvprintw(6, 60, cxp);
	mvprintw(6, 69, "/");
	mvprintw(6, 70, nxp);
}

void displayStrength(Player player){
	string strength;
	strength = to_string(player.getStrength());
	const char *cs = strength.c_str();
	mvprintw(8, 56, "STR:");
	mvprintw(8, 61, cs);
}

void displayAgility(Player player){
	string agility;
	agility = to_string(player.getAgility());
	const char *ca = agility.c_str();
	mvprintw(8, 64, "AGI:");
	mvprintw(8, 69, ca);
}

void displayIntelligence(Player player){
	string intelligence;
	intelligence = to_string(player.getIntelligence());
	const char *ci = intelligence.c_str();
	mvprintw(8, 71, "INT:");
	mvprintw(8, 76, ci);
}

void displayControls(){
	mvprintw(10, 63, "Controls:");
	mvprintw(11, 56, "Move:   Inv: i Look: l");
	mvprintw(12, 56, "7 8 9   Skills: p ");
	mvprintw(13, 56, "4 5 6   Magic: [");
	mvprintw(14, 56, "1 2 3   Help: ?  Esc: `");
}

void displayStandardUISuite(Player player, int turnCounter){
	clearUIPanel();
	drawUIDivisor();
	drawCombatLog();
	displayGeneral(player);
	displayTime(turnCounter);
	displayHealth(player);
	displayMana(player);
	displayXP(player);
	displayStrength(player);
	displayAgility(player);
	displayIntelligence(player);
	displayControls();
}
