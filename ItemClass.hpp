#ifndef Item_H
#define Item_H
#include "PlayerClass.hpp"
#include <functional>

enum class Grade{USELESS, COMMON, UNCOMMON, RARE, EXCEPTIONAL, LEGENDARY};
class Item{
private:
	char graphic;
	const char* name;
	const char* description;

public:
	Item(const char* name, const char* description, char graphic)
		:name(name),
		description(description),
		graphic(graphic)
	{}

	char getGraphic(){return graphic;}
	const char* getName(){return name;}
	const char* getDescription(){return description;}
};
		class ArmorItem: public Item{
		private:
			int armor;
			Grade grade;
			const char* slot;		
		public:
			ArmorItem(int armor, Grade grade, const char* slot, const char* name, const char* description, char graphic)
				:armor(armor),
				grade(grade),
				slot(slot),
				Item(name, description, graphic)
			{}

			int getArmor(){return armor;}
			Grade getGrade(){return grade;}
			const char* getSlot(){return slot;}
		};

			/*class Potion: public Item{
			public:
				Potion(const char* name, const char* description, char graphic)
					:Item(name, description, graphic)
				{}
				virtual void operator()(Player &player);
			};
				class PotionEffectHeal: public Potion{
					void operator()(Player &player){
						player.setHealth(player.getHealth() + player.getLevel() * 10);
					}
				};

				class PotionEffectHealMana: public Potion{
					void operator()(Player &player){
						player.setMana(player.getMaxMana());
					}
				};

				class PotionEffectMight: public Potion{

				};

				class PotionEffectTeleport: public Potion{

				};

				class PotionEffectOmniscience: public Potion{

				};*/

				


			class Ammunition: public Item{
			private:
				int damage;
				int stackSize;
			public:
				Ammunition(int damage, int stackSize, const char* name, const char* description, char graphic)
					:damage(damage),
					stackSize(stackSize),
					Item(name, description, graphic)
				{};
				int getDamage(){return damage;}
				int getStackSize(){return stackSize;}
				void setStackSize(int newSize){stackSize = newSize;}
				void decrementStackSize(){stackSize--;}
			};


		class Weapon: public Item{
		private:
			int diceNumber, diceSize, armorPen, critChance, accuracy;
			Grade grade;
			const char* type;	

		public:
			Weapon(int diceNumber, int diceSize, int armorPen, Grade grade, int critChance, int accuracy, const char* type,
				const char* name, const char* description, char graphic)
				:diceNumber(diceNumber),
				diceSize(diceSize),
				armorPen(armorPen),
				grade(grade),
				critChance(critChance),
				accuracy(accuracy),
				type(type),
				Item(name, description, graphic)
			{}	

			int getDiceNumber(){return diceNumber;}
			int getDiceSize(){return diceSize;}
			int getArmorPen(){return armorPen;}
			int getcritChance(){return critChance;}
			Grade getGrade(){return grade;}
		};

		class Shield: public Item{
			private:
				int armor, bashChance;
				Grade grade;
				const char* type;

			public:
				Shield(int armor, int bashChance, Grade grade, const char* type, const char* name, const char* description, char graphic)
					:armor(armor),
					bashChance(bashChance),
					grade(grade),
					type(type),
					Item(name, description, graphic)
				{}
				int getArmor(){return armor;}
				int getBashChance(){return bashChance;}
				Grade getGrade(){return grade;}
				const char* getType(){return type;}
		};

			class MissileWeapon: public Item{
			public:
				int accuracy, damage;
				Grade grade; 
				Ammunition projectile;
				MissileWeapon(int accuracy, int damage, Grade grade, Ammunition projectile, 
					const char* name, const char* description, char graphic):
					accuracy(accuracy),
					damage(damage),
					grade(grade),
					projectile(projectile),
					Item(name, description, graphic)
				{}
	 		};
#endif