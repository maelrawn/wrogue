#ifndef COLOR_H
#define COLOR_H
class Color{
private:
	int seenRval;
	int seenBval;
	int seenGval;
	int unseenRval;
	int unseenBval;
	int unseenGval;
public:
	Color(int rval, int bval, int gval)
		:rval(rval),
		bval(bval),
		gval(gval){
		this->unseenRval = rval / 2;
		this->unseenBval = bval / 2;
		this->unseenGval = gval / 2;
	}
	int getSeenRed(){return rval;}
	int getSeenBlue(){return bval;}
	int getSeenGreen(){return gval;}
	int	getUnseenRed(){return unseenRval;}
	int getUnseenBlue(){return unseenBval;}
	int getUnseenGreen(){return unseenGval;}
	void drawWithColor(int x, int y, char graphic, bool seen){
		if(seen)
			init_color(COLOR_MAGENTA, seenRval, seenGval, seenBval);
		else
			init_color(COLOR_MAGENTA, unseenRval, unseenGval, unseenBval);
		attron(COLOR_MAGENTA);
		mvaddch(y, x, graphic);
		attroff(COLOR_MAGENTA);
		init_color(COLOR_MAGENTA, 0, 250, 0);
	}
};
#endif