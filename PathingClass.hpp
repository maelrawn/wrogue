#ifndef PATHING_H
#define PATHING_H
#include "TileClass.hpp"

class PathingTile{
private:
	Tile tile;
	int anum;

public:	
	PathingTile(Tile tile, int anum)
		:tile(tile),
		anum(anum)
	{}
	Tile getTile(){return tile;}
	int getAnum(){return anum;} //No need for setters, only setting is during initialization with the custom ctor
	bool operator==(const PathingTile& other){
		bool lean = false;
		if(this->tile == other.tile)
			return true;
		else
			return false;
	}
};
#endif