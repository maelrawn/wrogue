//What does the enemy class require?
//We need a way to track hitpoints, damage, given xp, location, ability cooldowns, check names and descriptions, and active status.
//We also need a way for them to path to the player. This could be accomplished with an improved pathfinding algorithm.
//There also needs to be a way for them to attack, and to cast abilities. 
#ifndef ENEMY_H
#define ENEMY_H
#include <cmath>
#include "MapClass.hpp"
#include "CombatClass.hpp"
#include "PlayerClass.hpp"
#include "ItemClass.hpp"
#include "TileClass.hpp"
#include "PathingClass.hpp"
#include "LootClass.hpp"
class Map;
class Enemy{
private:
	bool active = false;
	bool seen = false;
	const char* name;
	const char* description;
	char graphic;
	int currenthealth;
	int maxhealth;
	int xp;
	int currentxpos;
	int	lastknownxpos;
	int currentypos;
	int lastknownypos;
	Weapon heldWeapon = emptyHand;
	Shield heldShield = noShield;
	int armor;
public:
	Enemy(){}
	Enemy(int maxhealth, int xp, int currentxpos, int currentypos, int armor,/* LootTable table,*/ const char* name, const char* description, char graphic)
		:maxhealth(maxhealth),
		currenthealth(maxhealth),
		xp(xp),
		currentxpos(currentxpos),
		currentypos(currentypos),
		armor(armor),
		name(name),
		description(description),
		graphic(graphic){
			//this->droppableItems = table;
	}
	int getHealth(){return currenthealth;}
	void setHealth(int newhp){currenthealth = newhp;}
	int getMaxHealth(){return maxhealth;}
	void setMaxHealth(int newmax){maxhealth = newmax;}
	int XPOnKill(){return xp;}
	int getCurrentX(){return currentxpos;}
	int getCurrentY(){return currentypos;}
	int getLastSeenX(){return lastknownxpos;}
	int getLastSeenY(){return lastknownypos;}
	const char* getName(){return name;}
	const char* getDescription(){return description;}
	char getGraphic(){return graphic;}
	bool checkSeen(Map &map, int xpos, int ypos){
		Tile tile = map.getTile(xpos, ypos);
		if(tile.getVisualStatus() && seen){
			active = true;
			lastknownypos = currentypos;
			lastknownxpos = currentxpos;
			return true;
		}
		else if(tile.getVisualStatus()){
			seen = true;
			lastknownypos = currentypos;
			lastknownxpos = currentxpos;
			return true;
		}
		else
			seen = false;

			return false;
	}
	bool isActive(){return active;}
	/*void pathToPlayer(Player player, Map& map){
		PathingTile currenttile = map.getPathingTile(currentxpos, currentypos);
		PathingTile endtile = map.getPathingTile(player.getXpos(), player.getYpos());
		Tile movedtotile;
		int distance = abs(endtile.getAnum() - currenttile.getAnum());
		int distance2;
		if(!map.getTile(currentxpos-1, currentypos-1).getCollision() && currentxpos >= 1 && currentypos >= 1){
			PathingTile northwest = map.getPathingTile(currentxpos-1, currentypos-1);
			distance2 = abs(endtile.getAnum() - northwest.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = northwest.getTile();
			}
		}
		if(!map.getTile(currentxpos, currentypos-1).getCollision() && currentypos >= 1){
			PathingTile north = map.getPathingTile(currentxpos, currentypos-1);
			distance2 = abs(endtile.getAnum() - north.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = north.getTile();
			}
		}
		if(!map.getTile(currentxpos+1, currentypos-1).getCollision() && currentxpos < map.getXmax() && currentypos >= 1){
			PathingTile northeast = map.getPathingTile(currentxpos+1, currentypos-1);
			distance2 = abs(endtile.getAnum() - northeast.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = northeast.getTile();
			}
		}
		if(!map.getTile(currentxpos-1, currentypos).getCollision() && currentxpos >= 1){
			PathingTile west = map.getPathingTile(currentxpos-1, currentypos);
			distance2 = abs(endtile.getAnum() - west.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = west.getTile();			
			}
		}
		if(!map.getTile(currentxpos+1, currentypos).getCollision() && currentxpos < map.getXmax()){
			PathingTile east = map.getPathingTile(currentxpos+1, currentypos);
			distance2 = abs(endtile.getAnum() - east.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = east.getTile();
			}
		}
		if(!map.getTile(currentxpos-1, currentypos+1).getCollision()){
			PathingTile southwest = map.getPathingTile(currentxpos-1, currentypos+1);
			distance2 = abs(endtile.getAnum() - southwest.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = southwest.getTile();
			}
		}
		if(!map.getTile(currentxpos, currentypos-1).getCollision()){
			PathingTile south = map.getPathingTile(currentxpos, currentypos-1);
			distance2 = abs(endtile.getAnum() - south.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = south.getTile();
			}
		}
		if(!map.getTile(currentxpos+1, currentypos+1).getCollision()){
			PathingTile southeast = map.getPathingTile(currentxpos+1, currentypos+1);
			distance2 = abs(endtile.getAnum() - southeast.getAnum());
			if(distance > distance2){
				distance = distance2;
				movedtotile = southeast.getTile();
			}
		}
		currentxpos = movedtotile.getXpos();
		currentypos = movedtotile.getYpos();				
	}*/
};
#endif