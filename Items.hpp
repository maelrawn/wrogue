	//ArmorItem item(armor, grade, slot, name, description, graphic)
	Grade useless = Grade::USELESS;
	Grade common = Grade::COMMON;
	Grade uncommon = Grade::UNCOMMON;
	Grade rare = Grade::RARE;
	Grade exceptional = Grade::EXCEPTIONAL;
	Grade legendary = Grade::LEGENDARY;
	ArmorItem bareHead(0, useless, "Head", "Head", "It's your head. There's nothing on it.", 'h');
	ArmorItem cardboardHat(1, useless, "Head", "Cardboard Hat", "It's some vaguely cubic cardboard.", 'h');
	ArmorItem leatherHat(2, common, "Head", "Leather Hat", "It's a leather skullcap. Feels nice, too.", 'h');
	ArmorItem tinfoilHat(3, uncommon, "Head", "Tinfoil Hat", "Tinfoil, in the shape of a paper boat, perched atop your head.", 'h');
	ArmorItem steelHat(4, rare, "Head", "Steel Helmet", "A centurion's helmet. Its red mane terrifies your foes.", 'h');
	ArmorItem boneHat(5, exceptional, "Head", "Discarded Skull", "A three-horned skull, picked clean by the scavengers.", 'h');
	ArmorItem hairHat(7, legendary, "Head", "Beautiful Wig", "This wig is absolutely gorgeous. You feel invulnerable wearing it.", 'h');
	ArmorItem gachiGASM(15, legendary, "Head", "gachiGASM", "Contorting your face seems to bend the rules of reality.", 'h');
	ArmorItem bareTorso(0, useless, "Torso", "Chest", "It's your torso. You're not wearing anything there.", 't');
	ArmorItem cardboardTorso(3, useless, "Torso", "Cardboard Box", "A cardboard box with some holes cut in it. You look like a scary robot!", 't');
	ArmorItem leatherTorso(6, common, "Torso", "Leather Armor", "Thick, boiled leather. It fits snugly around your torso.", 't');
	ArmorItem tinfoilTorso(9, uncommon, "Torso", "Tinfoil Armor", "Blind your enemies! Blind your friends! Blind yourself! It's Tinfoil Armor!", 't');
	ArmorItem steelTorso(12, rare, "Torso", "Steel Armor", "The abdomen is molded to resemble a steel six-pack. You feel confident.", 't');
	ArmorItem boneArmor(15, exceptional, "Torso", "Discarded Ribcage", "Ribs: Caged.", 't');
	ArmorItem klkTorso(20, legendary, "Torso", "Senketsu", "This skimpy sailor uniform couldn't possibly defend you from anything... could it?", 't');
	ArmorItem bareLegs(0, useless, "Legs", "Loincloth", "You've got something on, at least.", 'l');
	ArmorItem cardboardLegs(2, useless, "Legs", "Cardboard Pantaloons", "You found two boxes. How lucky!", 'l');
	ArmorItem leatherLegs(4, common, "Legs", "Leather Pantaloons", "These make your legs feel sort of scratchy, but they work.", 'l');
	ArmorItem tinfoilLegs(6, uncommon, "Legs", "Tinfoil Pantaloons", "Tinfoil Armor Co. cannot guarantee these pantaloons will not cut your arteries.", 'l');
	ArmorItem steelLegs(8, rare, "Legs", "Steel Greaves", "Shiny plates of steel protect your lower vitals.", 'l');
	ArmorItem boneLegs(10, exceptional, "Legs", "Small Discarded Ribcages", "It works just as well on your legs as in your torso!", 'l');
	ArmorItem codpiece(12, legendary, "Legs", "Codpiece", "It's like a condom, but it's your pants. Also, it's got a face on the end.", 'l');
	ArmorItem blueJeans(12, legendary, "Legs", "Levis", "Nothing is more rugged than a pair of blue jeans.", 'l');
	ArmorItem bareFeet(0, useless, "Feet", "Feet", "They're your feet. You've even kept all 10 toes, thus far.", 'f');
	ArmorItem cardboardFeet(1, useless, "Feet", "Cardboard Boots", "Something tells you you shouldn't step in water.", 'f');
	ArmorItem leatherFeet(2, common, "Feet", "Leather Boots", "They've got fur on the inside. Medieval Uggs?", 'f');
	ArmorItem tinfoilFeet(3, uncommon, "Feet", "Tinfoil Shoes", "Bedazzling footwear!", 'f');
	ArmorItem steelFeet(4, rare, "Feet", "Steel Boots", "They're steel-toed. Actually, they're all steel.", 'f');
	ArmorItem boneFeet(5, exceptional, "Feet", "Smallest Discarded Ribcages", "Does putting bones on things ever NOT work?", 'f');
	ArmorItem heelFeet(7, legendary, "Feet", "High Heels", "They look stunning with Senketsu.", 'f');
	ArmorItem wheelFeet(7, legendary, "Feet", "Little Wheels", "I don't know either, but you roll around now.", 'f');
	ArmorItem bareArms(0, useless, "Arms", "Arms", "They're your arms. Got some hair on them.", 'a');
	ArmorItem cardboardArms(2, useless, "Arms", "Cardboard Shoulderpads", "Like WoW shoulderpads, but significantly less useful.", 'a');
	ArmorItem leatherArms(4, common, "Arms", "Leather Vambraces", "Why don't more games have vambraces?", 'a');
	ArmorItem tinfoilArms(6, uncommon, "Arms", "Tinfoil Vambraces", "This is about as dangerous as your weapon.", 'a');
	ArmorItem steelArms(8, rare, "Arms", "Steel Vambraces", "They're thick and heavy. Too bad it's just on your arms.", 'a');
	ArmorItem boneArms(10, exceptional, "Arms", "Long Discarded Ribcages", "Wrapping things in bones is foolproof!", 'a');
	ArmorItem hairyArms(12, legendary, "Arms", "Hairy Arms", "You've sprouted hair! It's too thick to cut through. Ew.", 'a');
	ArmorItem bareHands(0, useless, "Hands", "Hands", "They're your hands. Still got all the digits, too.", 'g');
	ArmorItem cardboardHands(1, useless, "Hands", "Cardboard Gloves", "Crude, flimsy gauntlets.", 'g');
	ArmorItem leatherHands(2, common, "Hands", "Leather Gloves", "Your hands sweat a lot in these.", 'g');
	ArmorItem tinfoilHands(3, uncommon, "Hands", "Tinfoil Fingerhomes", "These gloves cut you more than the bad guys do!", 'g');
	ArmorItem steelHands(4, rare, "Hands", "Steel Gauntlets", "Some sturdy, proper gauntlets.", 'g');
	ArmorItem boneHands(5, exceptional, "Hands", "Bone Hands", "They're like skeleton gloves, but without the glove part. It's just a skeleton.", 'g');
	ArmorItem klkHands(7, legendary, "Hands", "Crimson Glove", "This crimson glove allows Senketsu to unleash great power.", 'g');
	//Weapon(int diceNumber, int diceSize, int armorPen, int grade, int critChance, int accuracy, const char* type,
			//const char* name, const char* description, char graphic)
	Weapon Axe_of_Debugging(10, 10, 10, legendary, 50, 100, "Axe", "Axe of Debugging", "Axe of the gods", 'a');
	Weapon Woodsplitter(10, 8, 20, legendary, 3, 85, "Axe", "Woodsplitter", "The legendary axe wielded by Paul Bunyan, but in miniature. Buy yours today!", 'a');
	Weapon GutsSword(3, 8, 1000, legendary, 10, 80, "Sword", "Dragonslayer Sword", "Massive, thick, heavy, and far too rough.", 's');
	Weapon klkSword(5, 6, 10, legendary, 30, 95, "Sword", "Scissor Sword", "One half of a crimson scissor, twice as long as your arm.", 's');
	Weapon cardboardSword(1, 6, 2, useless, 5, 90, "Sword", "Cardboard Tube", "This once was home to some toilet paper. Mighty, truly.", 's');
	Weapon Tentacle(10, 4, 0, legendary, 5, 90, "Sword", "Tentacle of Maelrawn", "Looking at this wriggling mass, you hear a quiet slurping noise.", 's');
	Weapon emptyHand(1, 4, 0, useless, 5, 80, "Generic", "Empty Hand", "You're not wielding anything in this hand!", 'w');
	Weapon plantSword(2, 4, 3, common, 5, 90, "Sword", "Dried-Out Vine", "This vine still has some spikes attached.", 's');
	//Weapon Swift_Blade(12, 6, 0, 30, 100, "Sword", "Swift Blade", "This sword feels as light as tinfoil and sharp as printer paper in your hands.", 's');
	Ammunition noAmmo(0, 0, "No Ammo", "You have no ammunition equipped!", 'p');
	MissileWeapon noMissileWeapon(0, 0, useless, noAmmo, "No Missile Weapon", "You have no missile weapon equipped!", 'b');

	/*LootTable depth0table
	LootTable depth1table
	LootTable depth2table
	LootTable depth3table
	LootTable depth4table
	LootTable depth5table
	LootTable depth6table
	LootTable depth7table
	LootTable depth8table
	LootTable depth9table
	LootTable depth10table*/