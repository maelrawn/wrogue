#ifndef LOOT_H
#define LOOT_H
#include <vector>
#include <typeinfo>
#include "MapClass.hpp"
#include "ItemClass.hpp"
#include "EnemyClass.hpp"
class LootTable{
private:
	int depth;
	vector<ArmorItem> armortable;
	vector<Weapon> weapontable;
	vector<MissileWeapon> missileweapontable;
	vector<Shield> shieldtable;
	//vector<Potion> potiontable; Can't turn this on until I make the potions work
	vector<Enemy> enemytable;
public:
	LootTable(int depth, vector<ArmorItem> copiedArmorTable, vector<Weapon> copiedWeaponTable, vector<Enemy> copiedEnemyTable,
		 vector<Shield> copiedshieldtable, /*vector<Potion> copiedpotiontable,*/ vector<MissileWeapon> copiedmissileweapontable)
		:depth(depth)
		{
			this->armortable = copiedArmorTable;
			this->weapontable = copiedWeaponTable;
			this->enemytable = copiedEnemyTable;
			this->shieldtable = copiedshieldtable;
//			this->potiontable = copiedpotiontable;
			this->missileweapontable = copiedmissileweapontable;
	}
	int getDepth(){return depth;}
	template<class genType>
	genType rollItem(){
		int num = rand() % 100; // Will have to adjust this for potions. Just shrink the missileweapon range.
		switch(num){
			case 0 ... 30:	
				return rollWeapon();
			case 31 ... 60:
				return rollArmor();
			case 61 ... 75:
				return rollShield();
			case 76 ... 99:
				return rollMissileWeapon();
		}	
	}
	Enemy rollEnemy(){
		int num = rand() % enemytable.size();
		return enemytable.at(num);
	}
	Weapon rollWeapon(){
		int num = rand() % weapontable.size();
		return weapontable.at(num);
	}
	MissileWeapon rollMissileWeapon(){
		int num = rand() % missileweapontable.size();
		return missileweapontable.at(num);
	}
	Weapon rollEnemyWeapon(){
		int num = rand() % weapontable.size();
		while(weapontable.at(num).getGrade() == Grade::LEGENDARY)
			num = rand() % weapontable.size();
		return weapontable.at(num);
	}	
	ArmorItem rollArmor(){
		int num = rand() % armortable.size();
		return armortable.at(num);
	}
	ArmorItem rollEnemyArmor(const char* slot){
		int num = rand() % armortable.size();
		while(armortable.at(num).getGrade() == Grade::LEGENDARY || armortable.at(num).getSlot() == slot)
			num = rand() % weapontable.size();
		return armortable.at(num);
	}	
	Shield rollShield(){
		int num = rand() % shieldtable.size();
		return shieldtable.at(num);
	}
	Shield rollEnemyShield(){
		int num = rand() % shieldtable.size();
		while(shieldtable.at(num).getGrade() == Grade::LEGENDARY)
			num = rand() % weapontable.size();
		return shieldtable.at(num);
	}
/*	Potion rollPotion(){
		int num = rand() % potiontable.size();
		return potiontable.at(num);
	}*/	
};
#endif