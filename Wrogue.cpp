#include <iostream>
#include <cstdlib>
#include <ncurses.h>
#include <random>
#include <stdio.h>
#include "UIClass.hpp"
#include "MapClass.hpp"
#include "TileClass.hpp"
#include "PlayerClass.hpp"
//#include "EnemyClass.hpp"
using namespace std;

void NWmove(Player &player, Map map);	//movement checking junk
void Nmove(Player &player, Map map);
void NEmove(Player &player, Map map);
void Wmove(Player &player, Map map);
void Emove(Player &player, Map map);
void SWmove(Player &player, Map map);
void Smove(Player &player, Map map);
void SEmove(Player &player, Map map);
bool timeIncrement();
void fieldOfView(Player player, Map& map);
int debug(int y, int ch);


void drawTileMap(Map map);
void drawPlayer(Player player);

void spawnAxe(Weapon Axe_of_Debugging, Player player, Map& floorMap);


int turnCounter = 1, newturnCounter = 1, depth = 0, debugy = 0;	 //Global variables to check if an action actually happened
											//Stops you from skipping your turn by attempting an invalid action

int main()

{
	cout<<"Wrogue 0.0.0.0.0.0.0.1.3\nEnter your name: ";
	string playername;
	getline(cin, playername);
	const char *pname = playername.c_str();
	Player player('@', 23, 12, pname);

	srand(time(0));

	bool timeStep = false;
	char Input;
	initscr();				//This block of stuff starts Curses mode,
	cbreak();				//disables the input buffer (inputs sent immediately after keypress), 
	keypad(stdscr, true);	//allows nonstandard keyboard inputs,
	noecho();				//disables drawing inputs to the screen, 
	start_color();			//gives us pretty colors,
	curs_set(0);			//and disables the cursor graphic

	init_color(COLOR_YELLOW, 250, 250, 250);	//Defines the colors we'll use, weird names
	init_color(COLOR_CYAN, 0, 0, 250);
	init_color(COLOR_MAGENTA, 0, 250, 0);
	init_pair(1, COLOR_WHITE, COLOR_BLACK);		//Normal object color
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);	//Normal object unseen color
	init_pair(3, COLOR_RED, COLOR_BLACK);		//Firestuff color
	init_pair(4, COLOR_BLUE, COLOR_CYAN);		//Water color
	init_pair(5, COLOR_CYAN, COLOR_BLACK);		//Water unseen color
	init_pair(6, COLOR_GREEN, COLOR_BLACK);		//Greenstuff color
	init_pair(7, COLOR_MAGENTA, COLOR_BLACK);	//Unseen greenstuff color

	Map floorMap(55, 24);
	floorMap.generate(player.getXpos(), player.getYpos());

	fieldOfView(player, floorMap);
	drawTileMap(floorMap);
	drawPlayer(player);
	displayStandardUISuite(player, turnCounter);
	refresh();

	while(Input != '`'){	//This while loop is our input processing
		timeStep = false;
		Input = getch();
		switch(Input){
			case 'q':
				NWmove(player, floorMap);
				break;
			case 'w':
				Nmove(player, floorMap);
				break;
			case 'e':
				NEmove(player, floorMap);
				break;
			case 'a':
				Wmove(player, floorMap);
				break;
			case 's':
				newturnCounter++;
				break;
			case 'd':
				Emove(player, floorMap);
				break;
			case 'z':
				SWmove(player, floorMap);
				break;
			case 'x':
				Smove(player, floorMap);
				break;
			case 'c':
				SEmove(player, floorMap);
				break;
			case 'i':
				clearScreen();
				drawFullscreenBorder('x');
				break;
			/*case 'g':
				player.Player::getItems(floorMap);
				break;*/
			case 'p':
				spawnAxe(Axe_of_Debugging, player, floorMap);
				break;
			case '<':
				Tile tile = floorMap.getTile(player.getXpos(), player.getYpos());
				if(tile.getGraphic() == '<'){
					floorMap.generate(player.getXpos(), player.getYpos());
					depth++;
				}
				break;


		}

		timeStep = timeIncrement();
		fieldOfView(player, floorMap);
		drawTileMap(floorMap);
		drawPlayer(player);
	}

	endwin();
}


//End of int main()
//Start of corollary functions

void fieldOfView(Player player, Map& map){
/* The way this is going to work is like this:
Check rays that end at the top edge of the vision square - y = constant = player.getYpos() - (distance player can see);
Check rays that end at the right edge of the vision square - x = constant = player.getXpos() + (distance player can see);
Check rays that end at the bottom edge of the vision square - y = constant = player.getYpos() + (distance player can see);
Check rays that end at the left edge of the vision square - x constant = player.getXpos() + (distance player can see);*/

//Resetting all CanSee bools so you lose vision of stuff you can't see
	float percent;
	for(int x = 0; x < map.getXmax(); x++){
    	for(int y = 0; y < map.getYmax(); y++){
    		Tile& tile = map.getTile(x, y);
    		tile.setVisualStatus(false);
    	}
    }
//Top edge rays: const y
    int limit = player.getXpos() + 10;
		while(limit >= map.getXmax())
			limit--;
	for(int xEndpoint = player.getXpos() - 10; xEndpoint <= limit; xEndpoint++){
		while(xEndpoint >= map.getXmax())
			xEndpoint--;
		while(xEndpoint <= 0)
			xEndpoint++;

			for(int z = 0; z < 11; z++){
				percent = 0.1 * z;
				int yEndpoint = player.getYpos() - 10;
				if(yEndpoint < 0)
					yEndpoint = 0;
				float pointYDirection = player.getYpos() + ((yEndpoint - player.getYpos())*percent) + .5;
				float pointXDirection = player.getXpos() + ((xEndpoint -player.getXpos())*percent) + .5;
				Tile& tile = map.getTile(pointXDirection, pointYDirection);
				tile.setVisualStatus(true);
				if(tile.getCollision() == true)
					z = 11;
			}
		}
//Right edge rays: const x
		 limit = player.getYpos() + 10;
		while(limit >= map.getYmax())
			limit--;
	for(int yEndpoint = player.getYpos() - 10; yEndpoint <= limit; yEndpoint++){
		while(yEndpoint <= 0)
			yEndpoint++;

			for(int z = 0; z < 11; z++){
				percent = 0.1 * z;
				int xEndpoint = player.getXpos() + 10;
					if(xEndpoint > map.getXmax())
						xEndpoint = map.getXmax() - 1;
				float pointYDirection = player.getYpos() + ((yEndpoint - player.getYpos())*percent) + .5;
				float pointXDirection = player.getXpos() + ((xEndpoint - player.getXpos())*percent) + .5;
				Tile& tile = map.getTile(pointXDirection, pointYDirection);
				tile.setVisualStatus(true);
				if(tile.getCollision() == true)
					z = 11;
			}
		}
//Bottom edge rays: const y
		 limit = player.getXpos() + 10;
		while(limit >= map.getXmax())
			limit--;
	for(int xEndpoint = player.getXpos() - 10; xEndpoint <= limit; xEndpoint++){
		while(xEndpoint >= map.getXmax())
			xEndpoint--;
		while(xEndpoint <= 0)
			xEndpoint++;
		
			for(int z = 0; z < 11; z++){
				percent = 0.1 * z;
				int yEndpoint = player.getYpos() + 10;
					if(yEndpoint >= map.getYmax())
						yEndpoint = map.getYmax()-1;//why is this the only one that needs to be this way? wtf?
				float pointYDirection = player.getYpos() + ((yEndpoint - player.getYpos())*percent) + .5;
				float pointXDirection = player.getXpos() + ((xEndpoint - player.getXpos())*percent) + .5;
				Tile& tile = map.getTile(pointXDirection, pointYDirection);
				tile.setVisualStatus(true);
				if(tile.getCollision() == true)
					z = 11;
			}
		}
//Left edge rays: const x
		limit = player.getYpos() + 10;
		while(limit >= map.getYmax())
			limit--;
	for(int yEndpoint = player.getYpos() - 10; yEndpoint <= limit; yEndpoint++){
		while(yEndpoint <= 0)
			yEndpoint++;

			for(int z = 0; z < 11; z++){
				percent = 0.1 * z;
				int xEndpoint = player.getXpos() - 10; 
					if(xEndpoint < 0)
						xEndpoint = 0;
				float pointYDirection = player.getYpos() + ((yEndpoint - player.getYpos())*percent) + .5;
				float pointXDirection = player.getXpos() + ((xEndpoint - player.getXpos())*percent) + .5;
				Tile& tile = map.getTile(pointXDirection, pointYDirection);
				tile.setVisualStatus(true);
				if(tile.getCollision() == true)
					z = 11;
			}
		}
	}



void drawTileMap(Map map){

    int x, y;

    for(x = 0; x < map.getXmax(); x++){
        for(y = 0; y < map.getYmax(); y++){
        	mvaddch(y, x, ' ');
        	Tile tile = map.getTile(x,y);
            if(tile.getVisualStatus() or tile.getVisualHistory()){
            	attron(COLOR_PAIR(tile.getColor()));
            	mvaddch(y, x, map.getTile(x,y).getDrawnGraphic());
            	attroff(COLOR_PAIR(tile.getColor()));
            }
            
        }
    }
}


void drawPlayer(Player player){
    mvaddch(player.getYpos(), player.getXpos(), player.getGraphic());
	displayStandardUISuite(player, turnCounter);

}

void NWmove(Player &player, Map map){	//Start movement checking junk - should I refactor this so it can be used with enemies?
	if((player.getYpos() - 1 != -1) and (player.getXpos() - 1 != -1) and (map.collision(player.getXpos() - 1, player.getYpos() - 1) == false)){	
		player.setXpos(player.getXpos() - 1);
		player.setYpos(player.getYpos() - 1);
		newturnCounter++;
	}
}

void Nmove(Player &player, Map map){
	if(player.getYpos() - 1 != -1 and map.collision(player.getXpos(), player.getYpos() - 1) != true){
		player.setYpos(player.getYpos() - 1);
		newturnCounter++;
	}
}

void NEmove(Player &player, Map map){
	if(player.getYpos() - 1 != -1 and player.getXpos() + 1 != map.getXmax() and map.collision(player.getXpos() + 1, player.getYpos() - 1) != true){
		player.setXpos(player.getXpos() + 1);
		player.setYpos(player.getYpos() - 1);
		newturnCounter++;
	}
}
void Wmove(Player &player, Map map){
	if(player.getXpos() - 1 != -1 and map.collision(player.getXpos() - 1, player.getYpos()) != true){
		player.setXpos(player.getXpos() - 1);
		newturnCounter++;
	}
}
void Emove(Player &player, Map map){
	if(player.getXpos() + 1 != map.getXmax() and map.collision(player.getXpos() + 1, player.getYpos()) != true){
		player.setXpos(player.getXpos() + 1);
		newturnCounter++;
	}
}
void SWmove(Player &player, Map map){
	if(player.getXpos() - 1 != -1 and player.getYpos() + 1!= map.getYmax() and map.collision(player.getXpos() - 1, player.getYpos() + 1) != true){
		player.setXpos(player.getXpos() - 1);
		player.setYpos(player.getYpos() + 1);
		newturnCounter++;
	}	
}
void Smove(Player &player, Map map){
	if(player.getYpos() + 1 != map.getYmax() and map.collision(player.getXpos(), player.getYpos() + 1) != true){
		player.setYpos(player.getYpos() + 1);
		newturnCounter++;
	}
}
void SEmove(Player &player, Map map){
	if(player.getYpos() + 1 != map.getYmax() and player.getXpos() + 1 != map.getXmax() and map.collision(player.getXpos() + 1, player.getYpos() + 1) != true){
		player.setXpos(player.getXpos() + 1);
		player.setYpos(player.getYpos() + 1);
		newturnCounter++;
	}
}	//End movement checking junk

bool timeIncrement(){	
	if(newturnCounter > turnCounter){										
		turnCounter = newturnCounter;				//Any *VALID* action the player can take will increment newturnCounter
		return true;								//actions of other entities are executed if this function is true
	}												//stops, for example, you getting hit in melee combat if you tried
	else											//to move into a space occupied by a wall while standing next to 
		return false;								//an opponent.
}

int debug(int y, int ch){
		mvprintw(50 + y, 0, ".");
		cout<<"       "<<ch;
		y++;
		refresh();
		return y;
}

void spawnAxe(Weapon Axe_of_Debugging, Player player, Map& floorMap){
    Tile& tile = floorMap.getTile(player.getXpos(), player.getYpos());
    
    tile.addItemToStack(Axe_of_Debugging);
    newturnCounter++;
}