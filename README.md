Welcome to Wrogue, also known as Worse Rogue!

Wrogue is a traditional roguelike made exclusively by myself as my first project. 
(Note: exclusively here means "with a lot of help fixing the stuff I break from my great friends.")

Roguelikes as a whole use traditional turn-based RPG mechanics, and oftentimes thematics as well.
In Wrogue, the goal is to descend the dungeon to defeat the arch-lich Odlarbej and claim the Amulet of Odlarbej as yours!

To build this yourself, you'll need ncurses 6.0. You can get this with `sudo apt install ncurses-base`. 
Once you have that, you can build the game by running build.sh.

If you encounter bugs, let me know at jdelaura00@gmail.com.